import Model.*;

import java.util.ArrayList;
import java.util.LinkedList;

import Utils.Constants;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class MetroAlgorithmsTest implements Constants {

    Graph<Station,String> testGraph = new Graph<>(false);

    @Before
    public void setUp() throws Exception {



    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Test
    public void a1_test_correct(){

        a1_test_correct_insert_data();
        assertEquals(testGraph, MetroAlgorithms.a1(COORDINATES_TEST_CSV_FILENAME, LINES_AND_STATIONS_TEST_CSV_FILENAME, CONNECTIONS_TEST_CSV_FILENAME));

    }

    private void a1_test_correct_insert_data(){

        Station sJoao = new Station("S.Joao");
        Station ipo = new Station("IPO");
        Station fariaGuimaraes = new Station("Faria Guimaraes");
        Station trindade = new Station("Trindade");
        Station aliados = new Station("Aliados");
        Station saoBento = new Station("Sao Bento");
        Station campanha = new Station("Campanha");
        Station bolhao = new Station("Bolhao");
        Station lapa = new Station("Lapa");
        Station casaDaMusica = new Station("Casa da Musica");
        Station ismai = new Station ("ISMAI");Station senhorDeMatosinhos = new Station("Senhor de Matosinhos");
        Station vilaDoConde = new Station("Vila do Conde");

        testGraph = new Graph<>(false);

        testGraph.insertVertex(sJoao);
        testGraph.insertVertex(ipo);
        testGraph.insertVertex(fariaGuimaraes);
        testGraph.insertVertex(trindade);
        testGraph.insertVertex(aliados);
        testGraph.insertVertex(saoBento);
        testGraph.insertVertex(campanha);
        testGraph.insertVertex(bolhao);
        testGraph.insertVertex(lapa);
        testGraph.insertVertex(casaDaMusica);
        testGraph.insertVertex(ismai);
        testGraph.insertVertex(senhorDeMatosinhos);
        testGraph.insertVertex(vilaDoConde);

        testGraph.insertEdge(sJoao, ipo, "1", 1);
        testGraph.insertEdge(ipo, fariaGuimaraes, "1", 2);
        testGraph.insertEdge(fariaGuimaraes, trindade, "1", 3);
        testGraph.insertEdge(trindade, aliados, "1", 5);
        testGraph.insertEdge(aliados, saoBento, "1", 8);
        testGraph.insertEdge(campanha, bolhao, "2", 10);
        testGraph.insertEdge(bolhao, trindade, "2", 3);
        testGraph.insertEdge(trindade, lapa, "2", 4);
        testGraph.insertEdge(lapa, casaDaMusica, "2", 4);
        testGraph.insertEdge(casaDaMusica, ismai, "2", 20);
        testGraph.insertEdge(campanha, bolhao, "3", 10);
        testGraph.insertEdge(bolhao, trindade, "3", 3);
        testGraph.insertEdge(trindade, lapa, "3", 4);
        testGraph.insertEdge(lapa, casaDaMusica, "3", 4);
        testGraph.insertEdge(casaDaMusica, senhorDeMatosinhos, "3", 100);
        testGraph.insertEdge(campanha, bolhao, "4", 10);
        testGraph.insertEdge(bolhao, trindade, "4", 3);
        testGraph.insertEdge(trindade, lapa, "4", 4);
        testGraph.insertEdge(lapa, casaDaMusica, "4", 4);
        testGraph.insertEdge(casaDaMusica, vilaDoConde, "4", 100);

    }

     @Test
     public void a2_testIsConexo(){
        String coordinatesFileName = "coordinates_test.csv";
        String linesAndStationsFileName = "lines_and_stations_test.csv";
        String connectionsFileName = "connections_test.csv";
        testGraph = MetroAlgorithms.a1(coordinatesFileName, linesAndStationsFileName, connectionsFileName);

        ArrayList<LinkedList<Station>> componentesTotalTest = MetroAlgorithms.a2_checkConexo(testGraph);

        assertEquals(componentesTotalTest, null);
        
    }

    @Test
    public void a2_testIsNotConexo(){
        String coordinatesFileName = "coordinates_testNotConexo.csv";
        String linesAndStationsFileName = "lines_and_stations_testNotConexo.csv";
        String connectionsFileName = "connections_test.csv";
        testGraph = MetroAlgorithms.a1(coordinatesFileName, linesAndStationsFileName, connectionsFileName);
        
        ArrayList<LinkedList<Station>> componentesTotalTest = MetroAlgorithms.a2_checkConexo(testGraph);
        if(componentesTotalTest.size() >=2){
            assertTrue(true);
        }

    }


    @Test
    public void a4_test_correct(){

        Graph<Station, String> graph = MetroAlgorithms.a1(COORDINATES_TEST_CSV_FILENAME, LINES_AND_STATIONS_TEST_CSV_FILENAME, CONNECTIONS_TEST_CSV_FILENAME);
        LinkedList<Station> caminhoTest = new LinkedList<>();
        caminhoTest.add(getStation(graph,"S.Joao"));
        caminhoTest.add(getStation(graph, "IPO"));
        caminhoTest.add(getStation(graph, "Faria Guimaraes"));
        caminhoTest.add(getStation(graph, "Trindade"));
        caminhoTest.add(getStation(graph, "Lapa"));
        assertEquals(caminhoTest, MetroAlgorithms.a4_shortestPathByStations(graph, getStation(graph, "S.Joao"), getStation(graph, "Lapa")).getCaminho());

    }

    @Test
    public void a5_test_correct(){

        Graph<Station, String> graph = MetroAlgorithms.a1(COORDINATES_TEST_CSV_FILENAME, LINES_AND_STATIONS_TEST_CSV_FILENAME, CONNECTIONS_TEST_CSV_FILENAME);
        LinkedList<Station> caminhoTest = new LinkedList<>();
        caminhoTest.add(getStation(graph,"S.Joao"));
        caminhoTest.add(getStation(graph, "IPO"));
        caminhoTest.add(getStation(graph, "Faria Guimaraes"));
        caminhoTest.add(getStation(graph, "Trindade"));
        caminhoTest.add(getStation(graph, "Lapa"));
        assertEquals(caminhoTest, MetroAlgorithms.a5_shortestPathByWeight(graph, getStation(graph, "S.Joao"), getStation(graph, "Lapa")).getCaminho());

    }

    @Test
    public void a6_test_correct(){

        Graph<Station, String> graph = MetroAlgorithms.a1(COORDINATES_TEST_CSV_FILENAME_AVEIRO, LINES_AND_STATIONS_TEST_CSV_FILENAME_AVEIRO, CONNECTIONS_TEST_CSV_FILENAME_AVEIRO);
        LinkedList<Station> caminhoTest = new LinkedList<>();
        caminhoTest.add(getStation(graph,"S.Joao"));
        caminhoTest.add(getStation(graph, "IPO"));
        caminhoTest.add(getStation(graph, "Faria Guimaraes"));
        caminhoTest.add(getStation(graph, "Aveiro"));
        caminhoTest.add(getStation(graph, "Lapa"));
        LinkedList<Station> caminhoSend = new LinkedList<>();
        MetroAlgorithms.a6_shortestPathLineChange(graph, getStation(graph, "S.Joao"), getStation(graph, "Lapa"), caminhoSend);
        assertEquals(caminhoTest, caminhoSend);

    }

    private Station getStation(Graph<Station, String> graph, String stationName){

        for(Station station : graph.vertices()){

            if(station.getName().equals(stationName))
                return station;

        }

        return null;

    }

    @Test
    public void a7_test(){

        Graph<Station, String> graph = new Graph<>(false);
        Station eA = new Station("A", 32.254, 515.221);
        Station eB = new Station("B", 12.1213, 2112.2);
        Station eC = new Station("C", 1233.258, 14.25);
        Station eD = new Station("D", 25.36, 85.96);
        Station eX = new Station("X", 74.85, 85.12);
        eA.addLine("1");
        eB.addLine("1");
        eC.addLine("1");
        eD.addLine("1");
        eX.addLine("1");

        graph.insertVertex(eA);
        graph.insertVertex(eB);
        graph.insertVertex(eC);
        graph.insertVertex(eD);
        graph.insertVertex(eX);

        graph.insertEdge(eA, eB, "1", 1);
        graph.insertEdge(eA, eC, "1", 100);
        graph.insertEdge(eB, eD, "1", 100);
        graph.insertEdge(eB, eC, "1", 1);
        graph.insertEdge(eB, eX, "1", 100);
        graph.insertEdge(eC, eD, "1", 1);
        graph.insertEdge(eC, eX, "1", 100);
        graph.insertEdge(eD, eX, "1", 1);

        Station eOrig = eA;
        Station eDest = eX;

        LinkedList<Station> estIntermedias = new LinkedList<>();
        estIntermedias.add(eC);
        estIntermedias.add(eB);
        estIntermedias.add(eD);

        LinkedList caminho = new LinkedList<>();
        caminho.add(eA);
        caminho.add(eB);
        caminho.add(eC);
        caminho.add(eD);
        caminho.add(eX);
        Percurso expResult = new Percurso(graph, caminho, 0);
        Percurso result = MetroAlgorithms.a7_shortestPathIntermediate(graph, eOrig, eDest, estIntermedias, 0);
        assertEquals(expResult.getCaminho(), result.getCaminho());

    }

}
