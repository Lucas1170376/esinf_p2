import Model.Connection;
import Utils.Reader;
import Model.Station;
import org.junit.AfterClass;
import org.junit.Before;

import java.util.*;

import org.junit.Test;

import static org.junit.Assert.*;

public class ReaderTest {

    List<String> lines = new ArrayList<>();
    List<String> stations = new ArrayList<>();
    List<Station> m_stations = new ArrayList<>();
    List<Connection> m_connections = new ArrayList<>();

    public ReaderTest() {
    }

    @Before
    public void setUp() throws Exception {

        m_connections.add(new Connection("S.Joao", "IPO", "1", 1));
        m_connections.add(new Connection("IPO", "Faria Guimaraes", "1", 2));
        m_connections.add(new Connection("Faria Guimaraes", "Trindade", "1", 3));
        m_connections.add(new Connection("Trindade", "Aliados", "1", 5));
        m_connections.add(new Connection("Aliados", "Sao Bento", "1", 8));
        m_connections.add(new Connection("Campanha", "Bolhao", "2", 10));
        m_connections.add(new Connection("Bolhao", "Trindade", "2", 3));
        m_connections.add(new Connection("Trindade", "Lapa", "2", 4));
        m_connections.add(new Connection("Lapa", "Casa da Musica", "2", 4));
        m_connections.add(new Connection("Casa da Musica", "ISMAI", "2", 20));
        m_connections.add(new Connection("Campanha", "Bolhao", "3", 10));
        m_connections.add(new Connection("Bolhao", "Trindade", "3", 3));
        m_connections.add(new Connection("Trindade", "Lapa", "3", 4));
        m_connections.add(new Connection("Lapa", "Casa da Musica", "3", 4));
        m_connections.add(new Connection("Casa da Musica", "Senhor de Matosinhos", "3", 20));
        m_connections.add(new Connection("Campanha", "Bolhao", "4", 10));
        m_connections.add(new Connection("Bolhao", "Trindade", "4", 3));
        m_connections.add(new Connection("Trindade", "Lapa", "4", 4));
        m_connections.add(new Connection("Lapa", "Casa da Musica", "4", 4));
        m_connections.add(new Connection("Casa da Musica", "Vila do Conde", "4", 100));

        lines.add("1"); stations.add("S.Joao");
        lines.add("1"); stations.add("IPO");
        lines.add("1"); stations.add("Faria Guimaraes");
        lines.add("1"); stations.add("Trindade");
        lines.add("2"); stations.add("Trindade");
        lines.add("3"); stations.add("Trindade");
        lines.add("4"); stations.add("Trindade");
        lines.add("1"); stations.add("Aliados");
        lines.add("1"); stations.add("Sao Bento");
        lines.add("2"); stations.add("Campanha");
        lines.add("3"); stations.add("Campanha");
        lines.add("4"); stations.add("Campanha");
        lines.add("2"); stations.add("Bolhao");
        lines.add("2"); stations.add("Lapa");
        lines.add("2"); stations.add("Casa da Musica");
        lines.add("3"); stations.add("Bolhao");
        lines.add("4"); stations.add("Bolhao");
        lines.add("3"); stations.add("Lapa");
        lines.add("4"); stations.add("Lapa");
        lines.add("3"); stations.add("Casa da Musica");
        lines.add("4"); stations.add("Casa da Musica");
        lines.add("2"); stations.add("ISMAI");
        lines.add("3"); stations.add("Senhor de Matosinhos");
        lines.add("4"); stations.add("Vila do Conde");

        m_stations.add(new Station("S.Joao"));
        m_stations.add(new Station("IPO"));
        m_stations.add(new Station("Faria Guimaraes"));
        m_stations.add(new Station("Trindade"));
        m_stations.add(new Station("Aliados"));
        m_stations.add(new Station("Sao Bento"));
        m_stations.add(new Station("Campanha"));
        m_stations.add(new Station("Bolhao"));
        m_stations.add(new Station("Lapa"));
        m_stations.add(new Station("Casa da Musica"));
        m_stations.add(new Station("ISMAI"));
        m_stations.add(new Station("Senhor de Matosinhos"));
        m_stations.add(new Station("Vila do Conde"));

    }

    @AfterClass
    public static void tearDownClass() {

    }

    @Test
    public void readLinesAndStationsCSVTestCorrect(){
        List<String> newLines = new ArrayList<>();
        List<String> newStations = new ArrayList<>();
        Reader.readLinesAndStationsCSV("lines_and_stations_test.csv", newLines, newStations);
        assertTrue(newLines.equals(lines) && newStations.equals(stations));
    }

    @Test
    public void readCoordinatesTestCorrect(){
        List<Station> stations = Reader.readCoordinatesCSV("coordinates_test.csv");
        assertEquals(stations, m_stations);
    }

    @Test
    public void readCoordinatesTestNoFileFound(){
        List<Station> stations = Reader.readCoordinatesCSV("uma_coisa.csv");
        if(stations == null){
            assertTrue(true);
        } else {
            assertTrue(false);
        }
    }

    @Test
    public void readConnectionTestCorrect(){
        List<Connection> connections = Reader.readConnection("connections_test.csv");
        if(connections.size() != m_connections.size())
            assertTrue(false);
        for (int i = 0; i < connections.size(); i++) {
            assertTrue(connections.get(i).equals(m_connections.get(i)));
        }
    }

}



