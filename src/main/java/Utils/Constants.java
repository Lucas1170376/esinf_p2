package Utils;

public interface Constants {

    String COORDINATES_TEST_CSV_FILENAME = "coordinates_test.csv";
    String LINES_AND_STATIONS_TEST_CSV_FILENAME = "lines_and_stations_test.csv";
    String CONNECTIONS_TEST_CSV_FILENAME = "connections_test.csv";
    String COORDINATES_CSV_FILENAME = "coordinates.csv";
    String LINES_AND_STATIONS_CSV_FILENAME = "lines_and_stations.csv";
    String CONNECTIONS_CSV_FILENAME = "connections.csv";
    String CSV_READER_SEPARATOR = ";";
    int CONNECTIONS_CSV_LINE_LENGTH = 4;
    int CONNECTIONS_CSV_LINE_CODE_INDEX = 0;
    int CONNECTIONS_CSV_STATION1_INDEX = 1;
    int CONNECTIONS_CSV_STATION2_INDEX = 2;
    int CONNECTIONS_CSV_TIME_INDEX = 3;
    int LINES_AND_STATIONS_LINE_LENGTH = 2;
    int LINES_AND_STATIONS_CSV_LINE_CODE_INDEX = 0;
    int LINES_AND_STATIONS_CSV_STATION_NAME_INDEX = 1;
    int COORDINATES_CSV_LINE_LENGTH = 3;
    int COORDINATES_CSV_STATION_NAME_INDEX = 0;
    int COORDINATES_CSV_LATITUDE_INDEX = 1;
    int COORDINATES_CSV_LONGITUDE_INDEX = 2;
    String COORDINATES_TEST_CSV_FILENAME_AVEIRO = "coordinates_test_aveiro.csv";
    String LINES_AND_STATIONS_TEST_CSV_FILENAME_AVEIRO = "lines_and_stations_test_aveiro.csv";
    String CONNECTIONS_TEST_CSV_FILENAME_AVEIRO = "connections_test_aveiro.csv";

}
