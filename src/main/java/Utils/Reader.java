package Utils;


import Model.Connection;
import Model.Station;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Reader implements Constants{

    public static void readLinesAndStationsCSV(String fileName, List<String> lines, List<String> stations){

        try {
            Scanner scanner = new Scanner(new File(fileName));

            while(scanner.hasNext()){
                String line = scanner.nextLine();
                if(!line.isEmpty()){
                    String[] data = line.split(CSV_READER_SEPARATOR);
                    if(data.length == LINES_AND_STATIONS_LINE_LENGTH) {
                        String lineCode = data[LINES_AND_STATIONS_CSV_LINE_CODE_INDEX];
                        String stationName = data[LINES_AND_STATIONS_CSV_STATION_NAME_INDEX];
                        lines.add(lineCode);
                        stations.add(stationName);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static List<Station> readCoordinatesCSV(String fileName){

        try {
            Scanner scanner = new Scanner(new File(fileName));
            List<Station> stations = new ArrayList<>();
            while(scanner.hasNext()){
                String line = scanner.nextLine();
                if(!line.isEmpty()){
                    String[] data = line.split(CSV_READER_SEPARATOR);
                    if(data.length == COORDINATES_CSV_LINE_LENGTH){
                        String stationName = data[COORDINATES_CSV_STATION_NAME_INDEX];
                        double latitude = Double.parseDouble(data[COORDINATES_CSV_LATITUDE_INDEX]);
                        double longitude = Double.parseDouble(data[COORDINATES_CSV_LONGITUDE_INDEX]);
                        Station newStation = new Station(stationName, latitude, longitude);
                        stations.add(newStation);
                    }
                }
            }
            return stations;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;

    }

    public static List<Connection> readConnection(String fileName){
        try {
            Scanner scanner = new Scanner(new File(fileName));
            List<Connection> connections = new ArrayList<>();
            while(scanner.hasNext()){
                String line = scanner.nextLine();
                if(!line.isEmpty()){
                    String[] data = line.split(CSV_READER_SEPARATOR);
                    if(data.length == CONNECTIONS_CSV_LINE_LENGTH){
                        String lineCode = data[CONNECTIONS_CSV_LINE_CODE_INDEX];
                        String station1 = data[CONNECTIONS_CSV_STATION1_INDEX];
                        String station2 = data[CONNECTIONS_CSV_STATION2_INDEX];
                        int time = Integer.parseInt(data[CONNECTIONS_CSV_TIME_INDEX]);
                        Connection connection = new Connection(station1, station2, lineCode, time);
                        connections.add(connection);
                    }
                }
            }
            return connections;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;

    }

}
