package Model;

import java.util.*;

public class GraphAlgorithms {

    /**
     * Performs breadth-first search of a Graph starting in a Vertex
     * @param g Graph instance
     * @param vert information of the Vertex that will be the source of the search
     * @return qbfs a queue with the vertices of breadth-first search
     */
    public static<V,E> LinkedList<V> BreadthFirstSearch(Graph<V,E> g, V vert){

        if(!g.validVertex(vert))
            return null;

        LinkedList<V> resultQueue = new LinkedList<>();
        LinkedList<V> auxQueue = new LinkedList<>();

        boolean visited[] = new boolean[g.numVertices()];

        resultQueue.add(vert);
        auxQueue.add(vert);
        visited[g.getKey(vert)] = true;

        while(!auxQueue.isEmpty()){

            vert = auxQueue.pop();
            for(V vAdj : g.adjVertices(vert)){
                if(!visited[g.getKey(vAdj)]){
                    resultQueue.add(vAdj);
                    auxQueue.add(vAdj);
                    visited[g.getKey(vAdj)] = true;
                }
            }
        }

        return resultQueue;

    }

    /**
     * Performs depth-first search starting in a Vertex
     * @param g Graph instance
     * @param vOrig Vertex of graph g that will be the source of the search
     * @param visited set of discovered vertices
     * @param qdfs queue with vertices of depth-first search
     */
    private static<V,E> void DepthFirstSearch(Graph<V,E> g, V vOrig, boolean[] visited, LinkedList<V> qdfs){

        int index = g.getKey(vOrig);
        visited[index] = true;
        for(int i = 0; i < g.numVertices(); i++){
            V[] keyVerts = g.allkeyVerts();
            Edge<V,E> edge = g.getEdge(keyVerts[index], keyVerts[i]);
            if(edge != null && !visited[i]){
                qdfs.add(keyVerts[i]);
                DepthFirstSearch(g, keyVerts[i], visited, qdfs);
            }
        }

    }

    /**
     * @param g Graph instance
     * @param vert information of the Vertex that will be the source of the search
     * @return qdfs a queue with the vertices of depth-first search
     */
    public static<V,E> LinkedList<V> DepthFirstSearch(Graph<V,E> g, V vert){

        if(!g.validVertex(vert))
            return null;

        LinkedList<V> finalQueue = new LinkedList<>();
        finalQueue.add(vert);
        boolean[] knownVertices = new boolean[g.numVertices()];
        DepthFirstSearch(g, vert, knownVertices, finalQueue);
        return finalQueue;

    }

    /**
     * Returns all paths from vOrig to vDest
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param vDest Vertex that will be the end of the path
     * @param visited set of discovered vertices
     * @param path stack with vertices of the current path (the path is in reverse order)
     * @param paths ArrayList with all the paths (in correct order)
     */
    private static<V,E> void allPaths(Graph<V,E> g, V vOrig, V vDest, boolean[] visited,
                                      LinkedList<V> path, ArrayList<LinkedList<V>> paths){
        int initialIndex = g.getKey(vOrig);
        visited[initialIndex] = true;
        path.push(vOrig);
        for(V v : g.adjVertices(vOrig)){
            if(v.equals(vDest)){
                path.push(v);
                paths.add(revPath(path));
                path.pop();
            } else {
                if(!visited[g.getKey(v)]){
                    allPaths(g, v, vDest, visited, path, paths);
                }
            }
        }
        V v = path.pop();
        visited[g.getKey(v)] = false;
    }

    /**
     * @param g Graph instance
     * @param vOrig information of the Vertex origin
     * @param vDest information of the Vertex destination
     * @return paths ArrayList with all paths from voInf to vdInf
     */
    public static<V,E> ArrayList<LinkedList<V>> allPaths(Graph<V,E> g, V vOrig, V vDest){
        if(!g.validVertex(vOrig) || !g.validVertex(vDest)){
            return new ArrayList<>();
        }
        boolean[] visited = new boolean[g.numVertices()];
        LinkedList<V> path = new LinkedList<>();
        ArrayList<LinkedList<V>> paths = new ArrayList<>();
        allPaths(g,vOrig, vDest, visited, path, paths);
        return paths;
    }

    /**
     * Computes shortest-path distance from a source vertex to all reachable
     * vertices of a graph g with nonnegative edge weights
     * This implementation uses Dijkstra's algorithm
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param visited set of discovered vertices
     * @param pathKeys minimum path vertices keys
     * @param dist minimum distances
     */
    protected static <V, E> void shortestPathLength(Graph<V, E> g, V vOrig, V[] vertices,
                                                    boolean[] visited, int[] pathKeys, double[] dist) {
        int i;
        for (V v : g.vertices()) {
            i = g.getKey(vOrig);
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
            visited[i] = false;
        }
        dist[g.getKey(vOrig)] = 0;

        while (vOrig != null) {
            visited[g.getKey(vOrig)] = true;
            for (V v : g.adjVertices(vOrig)) {
                Edge edge = g.getEdge(vOrig, v);
                if (!visited[g.getKey(v)] && dist[g.getKey(v)] > dist[g.getKey(vOrig)] + edge.getWeight()) {
                    dist[g.getKey(v)] = dist[g.getKey(vOrig)] + edge.getWeight();
                    pathKeys[g.getKey(v)] = g.getKey(vOrig);
                }
            }
            vOrig = getVertMinDist(g, dist, visited);
        }
    }

    private static <V, E> V getVertMinDist(Graph<V, E> g, double[] dist, boolean[] visited) {
        double min = Double.MAX_VALUE;
        V aux = null;
        for (V v : g.vertices()) {
            if (!visited[g.getKey(v)] && dist[g.getKey(v)] < min) {
                aux = v;
                min = dist[g.getKey(v)];
            }
        }
        return aux;
    }

    /**
     * Extracts from pathKeys the minimum path between voInf and vdInf
     * The path is constructed from the end to the beginning
     * @param g Graph instance
     * @param vDest information of the Vertex origin
     * @param vOrig information of the Vertex destination
     * @param pathKeys minimum path vertices keys
     * @param path stack with the minimum path (correct order)
     */
    protected static <V, E> void getPath(Graph<V, E> g, V vOrig, V vDest, V[] verts, int[] pathKeys, LinkedList<V> path) {

        path.clear();
        int orig = g.getKey(vOrig);
        int dest = g.getKey(vDest);
        int currentKey = g.getKey(vDest);
        path.addFirst(vDest);
        while (currentKey != orig) {
            currentKey = pathKeys[currentKey];
            for (V vertice : g.vertices()) {
                if (g.getKey(vertice) == currentKey) {
                    path.addFirst(vertice);
                }
            }
        }
    }

    public static <V, E> double shortestPath(Graph<V, E> g, V vOrig, V vDest, LinkedList<V> shortPath) {

        if (!g.validVertex(vOrig) || !g.validVertex(vDest)) {
            return 0;
        }

        int nverts = g.numVertices();
        boolean[] visited = new boolean[nverts];
        int[] pathKeys = new int[nverts];
        double[] dist = new double[nverts];
        V[] vertices = g.allkeyVerts();

        for (int i = 0; i < nverts; i++) {
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
        }

        shortestPathLength(g, vOrig, vertices, visited, pathKeys, dist);

        double lengthPath = dist[g.getKey(vDest)];
        if (lengthPath != Double.MAX_VALUE) {
            getPath(g, vOrig, vDest, vertices, pathKeys, shortPath);
            return lengthPath;
        }
        return 0;
    }

    //shortest-path between voInf and all other
    public static<V,E> boolean shortestPaths(Graph<V,E> g, V vOrig, ArrayList<LinkedList<V>> paths, ArrayList <Double> dists ){

        if (!g.validVertex(vOrig)) return false;

        int nverts = g.numVertices();
        boolean[] visited = new boolean[nverts];
        int[] pathKeys = new int[nverts];
        double[] dist = new double [nverts];
        V[] vertices = g.allkeyVerts();

        for (int i = 0; i < nverts; i++) {
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
        }

        shortestPathLength(g, vOrig, vertices, visited, pathKeys, dist);

        dists.clear(); paths.clear();
        for (int i = 0; i < nverts; i++) {
            paths.add(null);
            dists.add(null);
        }
        for (int i = 0; i < nverts; i++) {
            LinkedList <V> shortPath = new LinkedList<>();
            if (dist[i]!=Double.MAX_VALUE)
                getPath(g,vOrig,vertices[i],vertices,pathKeys,shortPath);
            paths.set(i, shortPath);
            dists.set(i, dist[i]);
        }
        return true;
    }

    /**
     * Reverses the path
     * @param path stack with path
     */
    private static<V,E> LinkedList<V> revPath(LinkedList<V> path){

        LinkedList<V> pathcopy = new LinkedList<>(path);
        LinkedList<V> pathrev = new LinkedList<>();

        while (!pathcopy.isEmpty())
            pathrev.push(pathcopy.pop());

        return pathrev ;
    }


    //shortest-path between voInf and all other
    public static boolean a6_2_shortestPathsLineChange(Graph<Station,String> g, Station vOrig, ArrayList<LinkedList<Station>> paths, ArrayList <Double> dists ){

        if (!g.validVertex(vOrig)) return false;

        int nverts = g.numVertices();
        boolean[] visited = new boolean[nverts];
        int[] pathKeys = new int[nverts];
        double[] dist = new double [nverts];
        Station[] vertices = g.allkeyVerts();

        for (int i = 0; i < nverts; i++) {
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
        }

        a6_3_shortestPathLengthLineChange(g, vOrig, visited, pathKeys, dist);

        dists.clear(); paths.clear();
        for (int i = 0; i < nverts; i++) {
            paths.add(null);
            dists.add(null);
        }
        for (int i = 0; i < nverts; i++) {
            LinkedList <Station> shortPath = new LinkedList<>();
            if (dist[i]!=Double.MAX_VALUE)
                getPath(g,vOrig,vertices[i],vertices,pathKeys,shortPath);
            paths.set(i, shortPath);
            dists.set(i, dist[i]);
        }
        return true;
    }

    public static void cycle(Graph<Station, String> graph, Station thisStation, boolean[] visited, int[]pathKeys, double[] dist){

        int thisStationIndex = graph.getKey(thisStation);

        for(Station station : graph.adjVertices(thisStation)){

            int stationIndex = graph.getKey(station);
            Edge<Station, String> edge = graph.getEdge(thisStation, station);
            String thisLine = edge.getElement();
            String lastLine = graph.getEdge(graph.allkeyVerts()[pathKeys[graph.getKey(thisStation)]], thisStation).getElement();
            int changesLine = thisLine.equals(lastLine) ? 0 : 1;
            if(!visited[stationIndex] && dist[stationIndex] >= dist[thisStationIndex] + changesLine){
                dist[stationIndex] = /*dist[thisStationIndex] +*/ changesLine;
                pathKeys[stationIndex] = thisStationIndex;
            }

        }

        thisStation = getVertMinDist(graph, dist, visited);
        if(thisStation != null) {
            visited[graph.getKey(thisStation)] = true;
            cycle(graph, thisStation, visited, pathKeys, dist);
        }

    }
    
    public static void a6_3_shortestPathLengthLineChange(Graph<Station, String> graph, Station thisStation, boolean[] visited, int[]pathKeys, double[] dist){
        
        int thisStationIndex = graph.getKey(thisStation);
        dist[thisStationIndex] = 0;
        pathKeys[thisStationIndex] = -1;
        visited[thisStationIndex] = true;

        for(Station adjStation : graph.adjVertices(thisStation)){

            int stationIndex = graph.getKey(adjStation);
            dist[stationIndex] = dist[thisStationIndex];
            pathKeys[stationIndex] = thisStationIndex;

        }

        thisStation = getVertMinDist(graph, dist, visited);
        if(thisStation != null){
            visited[graph.getKey(thisStation)] = true;
        }

        if(thisStation != null){
            visited[graph.getKey(thisStation)] = true;
            cycle(graph, thisStation, visited, pathKeys, dist);
        }

    }

}