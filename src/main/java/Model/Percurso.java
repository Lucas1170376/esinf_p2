package Model;

import java.util.*;

public class Percurso {

    private final List<String> linhas;
    private final List<Double> instances;
    private final LinkedHashMap<String, List<Station>> percursoEstacao;
    private final LinkedList<Station> caminho;

    public Percurso(Graph<Station,String> g, LinkedList<Station> caminho ,double instante) {

        this.caminho = caminho;
        instances = new ArrayList<>();
        linhas = a3_1_linhaPorPercurso(g,caminho);
        percursoEstacao = a3_2_estacoesPorLinha(g, caminho);
        a3_3_getInstantes(g, caminho, instante);
        
    }

    public LinkedList<Station> getCaminho() {
        return caminho;
    }

    public List<String> getLinhas() {
        return linhas;
    }

    public LinkedHashMap<String, List<Station>> getPercursoEstacao() {
        return percursoEstacao;
    }

    public List<Double> getInstances() {
        return instances;
    }

    public List<String> a3_1_linhaPorPercurso(Graph<Station,String> g , LinkedList<Station> caminho){
        
        ArrayList<String> linhasPercurso = new ArrayList<>();
        Iterator<Station>iterador = caminho.iterator();
       
        if(!iterador.hasNext()){
            return linhasPercurso;
        }
        
        Station estOrig = iterador.next();
        while (iterador.hasNext()){
            Station estDest = iterador.next();
            String linha = g.getEdge(estOrig, estDest).getElement();
            if (!linhasPercurso.contains(linha)){
                linhasPercurso.add(linha);
            }
            estOrig = estDest;
        }
        
        return linhasPercurso;
    }
    
    public LinkedHashMap<String, List<Station>> a3_2_estacoesPorLinha(Graph<Station,String> g, LinkedList<Station>caminho){
        
        LinkedHashMap<String, List<Station>> percursoEstacoes = new LinkedHashMap<>();
        Iterator<Station>iterador = caminho.iterator();
         
        if(!iterador.hasNext()){
            return percursoEstacoes;
        }
        
        Station estOrig = iterador.next();
        Map<String, ArrayList<Station>> instanteEstacao = new HashMap<>();
        
        for (String linha : linhas) {
             percursoEstacoes.put(linha, new ArrayList<Station>());
        }
       
        while (iterador.hasNext()){
            
            Station estDest = iterador.next();
            String linha = g.getEdge(estOrig, estDest).getElement();   
            for(String l : percursoEstacoes.keySet()){
                if (l.equals(linha)){
                    if(! percursoEstacoes.get(linha).contains(estOrig)){
                        percursoEstacoes.get(linha).add(estOrig);
                    }
                    percursoEstacoes.get(linha).add(estDest);
                }
                 
            }
            estOrig = estDest;
                  
        }

        return percursoEstacoes;

    }
    
    public void a3_3_getInstantes(Graph<Station,String> g,LinkedList<Station>caminho, double instante){

        Iterator<Station>iterador = caminho.iterator();
        
         if(!iterador.hasNext()){
            return ;
         }
          
        Station estOrig = iterador.next();
        instances.add(instante);

        while (iterador.hasNext()){
            Station estDest = iterador.next();
            double tempo = g.getEdge(estOrig, estDest).getWeight();
            instante = instante + tempo;
            instances.add(instante);
            estOrig = estDest;
        }

    }
    
}

