package Model;

public class Connection {

    private String station1;
    private String station2;
    private String line;
    private int time;

    public Connection(String station1, String station2, String line, int time){
        this.station1 = station1;
        this.station2 = station2;
        this.line = line;
        this.time = time;
    }

    public String getStation1() {
        return station1;
    }

    public String getStation2() {
        return station2;
    }

    public int getTime() {
        return time;
    }

    public String getLine() {
        return line;
    }

    public void setStation1(String station1) {
        this.station1 = station1;
    }

    public void setStation2(String station2) {
        this.station2 = station2;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void setLine(String line) {
        this.line = line;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(!(obj instanceof Connection))
            return false;
        Connection otherConnection = (Connection) obj;
        return time == otherConnection.time && line.equals(otherConnection.line) && station1.equals(otherConnection.station1) && station2.equals(otherConnection.station2);
    }
}
