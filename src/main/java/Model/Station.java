package Model;

import java.util.ArrayList;
import java.util.List;

public class Station {

    private String name;
    private List<String> lines;
    private double latitude;
    private double longitude;

    private static final double LATITUDE_BY_OMISSION = 0;
    private static final double LONGITUDE_BY_OMISSION = 0;

    public Station(String name, double latitude, double longitude){

        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        lines = new ArrayList<>();

    }

    public Station(String name){
        this.name = name;
        latitude = LATITUDE_BY_OMISSION;
        longitude = LONGITUDE_BY_OMISSION;
        lines = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<String> getLines() {
        return lines;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addLine(String line) {
        lines.add(line);
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == this)
            return true;
        if(!(obj instanceof Station))
            return false;
        Station other = (Station) obj;
        return other.name.equals(name);
    }

}
