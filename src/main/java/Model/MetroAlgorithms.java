package Model;

import Utils.Reader;

import javax.sound.sampled.Line;
import java.util.ArrayList;
import java.util.LinkedList;

import java.util.List;
import java.util.Map;

public class MetroAlgorithms {

    //reads files and returns the graph with the given information
    public static Graph<Station, String> a1(String coordinatesFileName, String linesAndStationsFileName, String connectionsFileName){
        Graph<Station,String> graph = new Graph<>(false);
        List<String> lines = new ArrayList<>();
        List<String> stations = new ArrayList<>();
        Reader.readLinesAndStationsCSV(linesAndStationsFileName, lines, stations);
        List<Station> coordinates = Reader.readCoordinatesCSV(coordinatesFileName);
        List<Connection> connections = Reader.readConnection(connectionsFileName);

        if(lines.isEmpty() || stations.isEmpty() || coordinates == null || connections == null)
            return null;

        for(Station station : coordinates){
            for(int i = 0; i < stations.size(); i++){
                if(stations.get(i).equals(station.getName())){
                    station.addLine(lines.get(i));
                }
            }
        }

        for(Station station : coordinates){
            graph.insertVertex(station);
        }

        for(Connection connection : connections){
            Station station1 = new Station("PlaceHolderStationName");
            Station station2 = new Station("PlaceHolderStationName");
            for(Station station : coordinates){
                if(station.getName().equals(connection.getStation1()))
                    station1 = station;
                if(station.getName().equals(connection.getStation2()))
                    station2 = station;
            }
            String line = connection.getLine();
            int time = connection.getTime();
            graph.insertEdge(station1, station2, line, time);
        }

        return graph;

    }
    
    /**
     * Returns the complents of the graph if it is not connected or returns null if it is connected
     * @param g graph instance
     * @return componentesTotal complements of the graph
     */
    public static ArrayList<LinkedList<Station>> a2_checkConexo(Graph<Station,String> g){
       
       ArrayList<LinkedList<Station>> componentesTotal = new ArrayList<>();
       boolean visited[] = new boolean[g.numVertices()];

          for (Station vertice : g.vertices()) {
              if(!visited[g.getKey(vertice)]){
                  LinkedList<Station> componente = GraphAlgorithms.BreadthFirstSearch(g, vertice);
                  componentesTotal.add(componente);
                  if (componente.size() == g.numVertices()){
                      return null;
                  }
                  for (Station station : componente) {
                      visited[g.getKey(station)] = true;
                  }
              }
          }       
       
       if (componentesTotal.size() >= 2){
           return componentesTotal;
       }else{
           return null;
       }
      
    }
    
    public static Percurso a4_shortestPathByStations(Graph<Station,String> g,Station estOrig, Station estDest){

        LinkedList<Station> paths = new LinkedList<>();
        Graph<Station,String> gClone = g.clone();

        for (Edge edge : gClone.edges()) {
            edge.setWeight(1);
        }

        GraphAlgorithms.shortestPath(gClone, estOrig, estDest, paths);
        
        Percurso percurso = new Percurso(g, paths, 0);

        return percurso;

    }


     public static Percurso a5_shortestPathByWeight(Graph<Station,String> g,Station estOrig, Station estDest){

        LinkedList<Station> paths = new LinkedList<>();
        GraphAlgorithms.shortestPath(g, estOrig, estDest, paths);
        Percurso percurso = new Percurso(g, paths, 0);
        return percurso;

    }
     
     public static double a6_shortestPathLineChange(Graph<Station, String> graph , Station initialStation, Station finalStation, LinkedList<Station> shortPath) {
        if (!graph .validVertex(finalStation) || !graph .validVertex(initialStation)){
            return 0;
        } else if (graph .getKey(finalStation) == graph .getKey(initialStation)){
            shortPath.add(finalStation);
            return 1;
        }
        shortPath.clear();
        ArrayList <LinkedList<Station>> paths = new ArrayList<>();
        ArrayList <Double> dists = new ArrayList <>();
        GraphAlgorithms.a6_2_shortestPathsLineChange(graph , initialStation, paths, dists);
        for (LinkedList<Station> path : paths) {
            if(path.size()> 2){
                if(graph .getKey(path.getFirst()) == graph .getKey(initialStation) && graph .getKey(finalStation) == graph .getKey(path.getLast())){
                    shortPath.addAll(path);
                    return dists.get(paths.indexOf(path));
                }
            }
        }
        return 0;
    }
     
     

    public static Percurso a7_shortestPathIntermediate(Graph<Station,String> graph, Station origin, Station destination, List<Station> intermediates, double instant){

        LinkedList<Station> finalPath = new LinkedList<>();
        a7_1_recursiveCall(graph, origin, destination, intermediates, finalPath);
        Percurso percurso = new Percurso(graph, finalPath, instant);
        return percurso;

    }

    public static void a7_1_recursiveCall(Graph<Station,String> graph, Station origin, Station destination, List<Station> intermediates, List<Station> finalPath){

        double shortestLenght = Double.MAX_VALUE;
        LinkedList<Station> pathToRun = new LinkedList<>();
        Station savedIntermediate = null;

        if(intermediates.size() == 0){

            LinkedList<Station> path = new LinkedList<>();
            GraphAlgorithms.shortestPath(graph, origin, destination, path);
            for(Station station : path){
                finalPath.add(station);
            }
            return;

        }

        for(Station intermediate : intermediates){
            LinkedList<Station> path = new LinkedList<>();
            double pathLength = GraphAlgorithms.shortestPath(graph, origin, intermediate, path);
            path.removeLast();
            if(pathLength < shortestLenght){
                savedIntermediate = intermediate;
                pathToRun = path;
                shortestLenght = pathLength;
            }
        }

        for(Station station : pathToRun){
            finalPath.add(station);
        }

        intermediates.remove(savedIntermediate);
        origin = savedIntermediate;

        if(origin.equals(destination))
            return;

        a7_1_recursiveCall(graph, origin, destination, intermediates, finalPath);

    }

}
